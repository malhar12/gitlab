import gql from 'graphql-tag';

/**
 * A GraphQL query building function
 *
 * @return {String} the parsed GraphQL query string
 */
export default () => {
  
    return gql`
    query($fullPath: ID!){
      project(
        fullPath: $fullPath
      ) {
          id
          issues {
              nodes {
                id
                iid
                epic{
                  iid
                }
                labels{
                  nodes{
                    id
                    title
                    color
                    description
                  }
                }
                title
                createdAt
                state
                milestone{
                  title
                }
                weight
                dueDate
                assignees(first:100){
                  nodes{
                    name
                    avatarUrl
                    webUrl
                    
                  }
                }
                iteration {
                  id
                }
                author {
                  name
                  webUrl
                  avatarUrl
                }
              }
            }
      }
    }
  `;
};
